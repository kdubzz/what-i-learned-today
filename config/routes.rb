Rails.application.routes.draw do
  resources :portfolios, except: [:show, :edit]

  get 'portfolio/:id', :to => 'portfolios#show', :as => 'portfolio_show'
  get 'portfolio/:id/edit', :to => 'portfolios#edit', :as => 'portfolio_edit'

  resources :blogs do
    member do
      get :toggle_status
    end
  end

  get 'about', :to => 'pages#about'
  get 'contact', :to => 'pages#contact'

  root 'pages#home'
end
