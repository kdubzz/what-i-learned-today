class PortfoliosController < ApplicationController
  before_action :get_portfolio_item, only: [:show, :edit, :update, :destroy]

  # GET /portfolios
  def index
    @portfolio_items = Portfolio.all
  end

  # GET /portfolios/1
  def show
  end

  # GET /portfolios/new
  def new
    @portfolio_item = Portfolio.new
  end

  # POST /portfolios
  def create
    @portfolio_item = Portfolio.new(portfolio_item_params)

    respond_to do |format|
      if @portfolio_item.save
        format.html { redirect_to @portfolio_item, :notice => 'Portfolio item was successfully created' }
      else
        format.html { render 'new' }
      end
    end
  end

  # GET /portfolios/1/edit
  def edit
  end

  # PATCH /portfolios/1
  def update
    respond_to do |format|
      if @portfolio_item.update(portfolio_item_params)
        format.html { redirect_to portfolio_show_path(@portfolio_item), :notice => 'Portfolio item was successfully updated' }
      else
        format.html { render 'edit' }
      end
    end
  end

  # DESTROY /portfolios/1
  def destroy
    @portfolio_item.destroy

    respond_to do |format|
      format.html { redirect_to portfolios_path, notice: 'Portfolio item was successfully removed!' }
    end
  end

  private
    def portfolio_item_params
      params.require(:portfolio)
        .permit(:title, :subtitle, :body)
    end

    def get_portfolio_item
      @portfolio_item = Portfolio.friendly.find(params[:id])
    end
end
