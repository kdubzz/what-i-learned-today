# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

require 'faker'

20.times do |post|
  Blog.create!(
    :title => Faker::Book.title,
    :body => Faker::Lorem.paragraph(:sentence_count => 5),
  )
end

puts '20 Blog posts seeded!'

5.times do |skill|
  Skill.create!(
    :title => Faker::ProgrammingLanguage.name,
    :percent_utilized => Faker::Number.between(:from => 1, :to => 100)
  )
end

puts '5 skill sets seeded!'

9.times do |portfolio_item|
  Portfolio.create!(
    :title => Faker::Superhero.name,
    :subtitle => Faker::Markdown.block_code,
    :body => Faker::Lorem.paragraph(:sentence_count => 7),
    :main_image => Faker::LoremFlickr.image(:size => '600x400', :search_terms => ['sports', 'fitness']),
    :thumb_image => Faker::LoremFlickr.image(:size => '350x200', :search_terms => ['sports', 'fitness'])
  )
end

puts '9 portfolio items seeded!'